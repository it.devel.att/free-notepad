package com.example.notepadfree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<Note> notes;
    private MainContract.Presenter presenter;

    public NoteAdapter(Context context, List<Note> notes, MainContract.Presenter presenter) {
        this.inflater = LayoutInflater.from(context);
        this.notes = notes;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public NoteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.note_item, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView noteTitle;
        final TextView editNoteDatetime;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            noteTitle = itemView.findViewById(R.id.noteItemTitle);
            editNoteDatetime = itemView.findViewById(R.id.noteItemLastEditDatetime);
        }

    }
    
    @Override
    public void onBindViewHolder(@NonNull NoteAdapter.ViewHolder holder, final int position) {
        Note note = notes.get(position);
        holder.noteTitle.setText(note.getTitle());
        holder.editNoteDatetime.setText(note.getEditDateTimeFormatString());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.editNoteActivityOpen(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }
}
