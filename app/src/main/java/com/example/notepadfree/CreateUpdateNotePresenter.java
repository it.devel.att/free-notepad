package com.example.notepadfree;

import android.content.Intent;
import android.os.Bundle;

public class CreateUpdateNotePresenter implements CreateUpdateNoteContract.Presenter {
    private final String TAG = this.getClass().getSimpleName();
    private CreateUpdateNoteContract.View view;
    private Note editNote;

    public void attachView(CreateUpdateNoteContract.View view) {
        this.view = view;
    }

    @Override
    public Note getNote() {
        return editNote;
    }

    @Override
    public void updateNote() {
        String title = view.getNoteTitle();
        String description = view.getNoteDescription();
        editNote.setTitle(title);
        editNote.setDescription(description);
        Intent data = new Intent();
        data.putExtra(Note.class.getSimpleName(), editNote);
        data.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        view.returnEditNoteToResult(data);
    }

    @Override
    public void saveNote() {
        String title = view.getNoteTitle();
        String description = view.getNoteDescription();
        Note newNote = new Note(title, description);
        Intent data = new Intent();
        data.putExtra(Note.class.getSimpleName(), newNote);
        data.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        view.returnNewNoteToResult(data);
    }

    @Override
    public void deleteNote() {
        Intent intent = new Intent();
        intent.putExtra(Note.class.getSimpleName(), editNote);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        view.returnDeleteNoteToResult(intent);
    }

    @Override
    public void shareNote() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, view.getNoteDescription());
        sendIntent.setType("text/plain");
        view.shareNoteStartActivity(sendIntent);
    }

    @Override
    public void setNote(Bundle data) {
        editNote = data.getParcelable(Note.class.getSimpleName());
        if (editNote != null) {
            view.setNoteTitle(editNote.getTitle());
            view.setNoteDescription(editNote.getDescription());
        }
    }


}
