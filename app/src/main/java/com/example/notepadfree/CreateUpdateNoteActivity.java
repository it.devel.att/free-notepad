package com.example.notepadfree;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class CreateUpdateNoteActivity extends AppCompatActivity implements CreateUpdateNoteContract.View {
    private final String TAG = this.getClass().getSimpleName();
    private CreateUpdateNoteContract.Presenter presenter = new CreateUpdateNotePresenter();
    private static final int DELETE_NOTE = 3;

    private EditText noteTitle;
    private EditText noteDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_update_note);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        noteTitle = findViewById(R.id.noteTitle);
        noteDescription = findViewById(R.id.noteDescription);

        presenter.attachView(this);
        Bundle data = getIntent().getExtras();
        if (data != null) {
            presenter.setNote(data);
        }
        
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                noteDescription.setText(sharedText);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_note, menu);
        MenuItem deleteButton = menu.findItem(R.id.menuDeleteButton);
        if (presenter.getNote() == null) {
            deleteButton.setVisible(false);
        } else {
            int orientation = this.getResources().getConfiguration().orientation;

            if (orientation == Configuration.ORIENTATION_PORTRAIT)
                deleteButton.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            else
                deleteButton.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSaveButton:
                if (presenter.getNote() != null)
                    presenter.updateNote();
                else
                    presenter.saveNote();
                break;
            case R.id.menuShareButton:
                presenter.shareNote();
                break;
            case R.id.menuDeleteButton:
                presenter.deleteNote();
                break;
        }
        return true;
    }

    @Override
    public String getNoteTitle() {
        return noteTitle.getText().toString();
    }

    @Override
    public String getNoteDescription() {
        return noteDescription.getText().toString();
    }

    @Override
    public void setNoteTitle(String title) {
        noteTitle.setText(title);
    }

    @Override
    public void setNoteDescription(String description) {
        noteDescription.setText(description);
    }


    @Override
    public void returnNewNoteToResult(Intent data) {
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void returnEditNoteToResult(Intent data) {
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void returnDeleteNoteToResult(Intent data) {
        setResult(DELETE_NOTE, data);
        finish();
    }

    @Override
    public void shareNoteStartActivity(Intent intent) {
        startActivity(intent);
    }
}
