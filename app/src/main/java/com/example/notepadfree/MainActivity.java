package com.example.notepadfree;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainContract.View {
    private final String TAG = this.getClass().getSimpleName();

    private static final String PREFS_FILE = "freeNotes";
    private static final String PREF_NAME = "freeNotesJson";

    private final int CREATE_NEW_NOTE = 1;
    private final int EDIT_NOTE = 2;
    private final int DELETE_NOTE = 3;
    SharedPreferences settings;

    private RecyclerView noteListRecyclerView;
    private NoteAdapter noteAdapter;
    private MainContract.Presenter presenter = new NotesPresenter();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        presenter.attachView(this);

        noteListRecyclerView = findViewById(R.id.noteList);
        noteAdapter = new NoteAdapter(this, presenter.getNotes(), presenter);
        noteListRecyclerView.setAdapter(noteAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.newNoteCreateActivityOpen();
            }
        });
        settings = getSharedPreferences(PREFS_FILE, MODE_PRIVATE);
        String jsonNotes = settings.getString(PREF_NAME, null);
        if (jsonNotes != null) {
            presenter.setNotes(Note.toListNotes(jsonNotes));
            noteAdapter = new NoteAdapter(this, presenter.getNotes(), presenter);
            noteListRecyclerView.setAdapter(noteAdapter);
        }
    }

    @Override
    protected void onStop() {
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(PREF_NAME);
        editor.putString(PREF_NAME, Note.toJSONString(presenter.getNotes()));
        editor.apply();
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList("notes", new ArrayList<>(presenter.getNotes()));
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(PREF_NAME);
        editor.putString(PREF_NAME, Note.toJSONString(presenter.getNotes()));
        editor.apply();
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        List<Note> notes = savedInstanceState.getParcelableArrayList("notes");
        presenter.setNotes(notes);

        noteAdapter = new NoteAdapter(this, presenter.getNotes(), presenter);
        noteListRecyclerView.setAdapter(noteAdapter);
    }

    @Override
    public void openCreateNoteView() {
        Intent intent = new Intent(getApplicationContext(), CreateUpdateNoteActivity.class);
        startActivityForResult(intent, CREATE_NEW_NOTE);
    }

    @Override
    public void openEditNoteView(Note note) {
        Intent intent = new Intent(getApplicationContext(), CreateUpdateNoteActivity.class);
        intent.putExtra(Note.class.getSimpleName(), note);
        startActivityForResult(intent, EDIT_NOTE);
    }

    @Override
    public void reloadNotesList() {
        noteAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CREATE_NEW_NOTE && resultCode == RESULT_OK) {
            if (data != null) {
                presenter.addNote(data);
            }
        } else if (requestCode == EDIT_NOTE && resultCode == RESULT_OK) {
            if (data != null) {
                presenter.updateNote(data);
            }
        } else if (resultCode == DELETE_NOTE) {
            if (data != null) {
                presenter.removeNote(data);
            }
        }
    }


}
