package com.example.notepadfree;

import android.content.Intent;

import java.util.List;

public interface MainContract {
    interface View {
        void openCreateNoteView();

        void openEditNoteView(Note note);

        void reloadNotesList();
    }

    interface Presenter {
        void attachView(MainContract.View view);

        void newNoteCreateActivityOpen();

        void editNoteActivityOpen(int position);

        List<Note> getNotes();

        void setNotes(List<Note> notes);

        void addNote(Intent data);

        void updateNote(Intent data);

        void removeNote(Intent data);
    }
}
