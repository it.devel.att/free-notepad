package com.example.notepadfree;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class Note implements Parcelable {

    @NonNull
    private String uuid;

    private String title;
    private Date editDateTime;

    private String description;

    private static Gson gson = new Gson();
    private static Type notesListType = new TypeToken<List<Note>>() {
    }.getType();

    public Note(String title) {
        this.title = title;
        this.editDateTime = new Date();
        this.uuid = UUID.randomUUID().toString();
    }

    public Note(String title, String description) {
        this.title = title;
        this.description = description;
        this.editDateTime = new Date();
        this.uuid = UUID.randomUUID().toString();
    }

    public Note(Parcel in) {
        title = in.readString();
        long tmpEditDateTime = in.readLong();
        editDateTime = tmpEditDateTime != -1 ? new Date(tmpEditDateTime) : null;
        description = in.readString();
        uuid = in.readString();
    }

    public String getUuid() {
        return uuid;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getEditDateTimeFormatString() {
        return new SimpleDateFormat("d/M/yy HH:mm:ss", Locale.getDefault()).format(editDateTime);
    }

    public void setDescription(String description) {
        this.description = description;
        this.editDateTime = new Date();
    }

    public void setTitle(String title) {
        this.title = title;
        this.editDateTime = new Date();
    }


    public static List<Note> generateNotes() {
        List<Note> notes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            notes.add(new Note(String.format("Title %s", i)));
        }
        return notes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeLong(editDateTime != null ? editDateTime.getTime() : -1L);
        dest.writeString(description);
        dest.writeString(uuid);
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {

        @Override
        public Note createFromParcel(Parcel source) {
            return new Note(source);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public static String toJSONString(List<Note> notes) {
        return gson.toJson(notes);
    }

    public static List<Note> toListNotes(String json) {
        return gson.fromJson(json, notesListType);
    }
}
