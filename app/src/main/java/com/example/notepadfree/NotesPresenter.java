package com.example.notepadfree;

import android.content.Intent;

import java.util.Iterator;
import java.util.List;

public class NotesPresenter implements MainContract.Presenter {
    private final String TAG = this.getClass().getSimpleName();

    private MainContract.View view;
    private List<Note> notes = Note.generateNotes();

    public void attachView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void newNoteCreateActivityOpen() {
        view.openCreateNoteView();
    }

    @Override
    public void editNoteActivityOpen(int position) {
        Note note = notes.get(position);
        view.openEditNoteView(note);
    }

    @Override
    public List<Note> getNotes() {
        return notes;
    }

    @Override
    public void setNotes(List<Note> notes) {
        this.notes = notes;
        view.reloadNotesList();
    }

    @Override
    public void addNote(Intent data) {
        Note newNote = data.getParcelableExtra(Note.class.getSimpleName());
        notes.add(newNote);
        view.reloadNotesList();
    }

    @Override
    public void updateNote(Intent data) {
        Note updatedNote = data.getParcelableExtra(Note.class.getSimpleName());
        for (Note note : notes) {
            if (note.getUuid().equals(updatedNote.getUuid())) {
                notes.set(notes.indexOf(note), updatedNote);
            }
        }
        view.reloadNotesList();
    }

    @Override
    public void removeNote(Intent data) {
        final Note deleteNote = data.getParcelableExtra(Note.class.getSimpleName());
        for (Iterator<Note> iterator = notes.iterator(); iterator.hasNext(); ) {
            Note note = iterator.next();
            if (note.getUuid().equals(deleteNote.getUuid())) {
                iterator.remove();
            }
        }
        view.reloadNotesList();
    }
}
