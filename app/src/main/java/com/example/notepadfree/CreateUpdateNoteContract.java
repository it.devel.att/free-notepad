package com.example.notepadfree;

import android.content.Intent;
import android.os.Bundle;

public interface CreateUpdateNoteContract {
    interface View {
        String getNoteTitle();

        String getNoteDescription();

        void setNoteTitle(String title);

        void setNoteDescription(String description);

        void returnNewNoteToResult(Intent data);

        void returnEditNoteToResult(Intent data);

        void returnDeleteNoteToResult(Intent data);

        void shareNoteStartActivity(Intent intent);
    }

    interface Presenter {
        void attachView(CreateUpdateNoteContract.View view);

        Note getNote();

        void setNote(Bundle data);

        void updateNote();

        void saveNote();

        void deleteNote();

        void shareNote();
    }
}
